# Distributed under the OSI-approved BSD 2-Clause License.
#
# Copyright (C) 2020 DFTB+ developers group
#

#[=======================================================================[.rst:
FindCustomDbcsr
-------------------

Finds the DBCSR library.

This is a simple auto-detection module for the DBCSR library (looks
basically for a library with the name 'dbcsr'). It also assumes that
DBCSR is MPI-based and defines a respective dependency on
``MPI::MPI_Fortran.``


Imported Targets
^^^^^^^^^^^^^^^^

This module provides the following imported targets, if found:

``DBCSR::DBCSR``
  The DBCSR library.


Result Variables
^^^^^^^^^^^^^^^^

This module will define the following variable:

``DBCSR_FOUND``
  True if the system has the DBCSR library


Cache variables
^^^^^^^^^^^^^^^

The following cache variables may be set to influence the library detection:

``DBCSR_DETECTION``
  Whether DBCSR libraries should be detected (default: True). If set to False,
  the settings in ``DBCSR_LIBRARY`` will be used without any further checks.

``DBCSR_LIBRARY``
  Customized DBCSR library/libraries to use.  If no DBCSR library is
  required (e.g. the linker automatically links it) set
  ``DBCSR_LIBRARY="NONE"``. If not set or empty, it will use
  ``find_package(dbcsr)`` to find the dbcsr library (relying on the
  existence of a CMake export file for dbcsr). Otherwise, the listed
  libraries will be checked for existence (unless disabled in
  ``DBCSR_DETECTION``) and the variable is overwritten to contain the
  libraries with their with full path.

``DBCSR_LIBRARY_DIR``
  Directories which should be looked up in order to find the customized library.

``DBCSR_INCLUDE_DIR``
  Include directory.

``DBCSR_ROOT``
  DBCSR root directory.

``DBCSR_WITH_CUDA``
  Whether DBCSR uses GPUs through the CUDA library.

``DBCSR_WITH_CUDA_PROFILING``
  Whether DBCSR uses CUDA profiling.

#]=======================================================================]

include(FindPackageHandleStandardArgs)
include(CustomLibraryFinder)

# indent for cleaner output
message(STATUS "Parsing DBCSR options")
list(APPEND CMAKE_MESSAGE_INDENT "  ")

if(NOT "${DBCSR_ROOT}" STREQUAL "")
  set(DBCSR_INCLUDE_DIR "${DBCSR_ROOT}/include")
  set(DBCSR_LIBRARY_DIR "${DBCSR_ROOT}/lib")
  set(DBCSR_LIBRARY "${DBCSR_ROOT}/lib/libdbcsr.a")
endif()

if(TARGET DBCSR::DBCSR)
  set(CUSTOMDBSCR_FOUND True)
  set(CustomDbscr_FOUND True)
  set(DBCSR_FOUND True)
  set(Dbscr_FOUND True)
  message(STATUS "DBCSR already defined")

else()
  # We are already locating MPI at the top-level, or at least we should do that!
  find_package(MPI ${CustomDbscr_FIND_REQUIRED} QUIET)

  option(DBCSR_DETECTION "Whether DBCSR library should be detected" TRUE)
  message(CHECK_START "Locating DBCSR library")

  if(DBCSR_DETECTION)
    message(STATUS "Using user-defined variables")

    # ON = find_quietly
    find_custom_libraries("${DBCSRLIBRARY}" "${DBCSR_LIBRARY_DIR}" ON _libs)
    set(DBCSR_LIBRARY "${_libs}" CACHE STRING "List of DBCSR libraries to link" FORCE)
    unset(_libs)

    set(DBCSR_DETECTION False CACHE BOOL "Whether DBCSR libraries should be detected" FORCE)
  endif()

  find_package_handle_standard_args(CustomDbcsr REQUIRED_VARS DBCSR_LIBRARY MPI_Fortran_FOUND)

  set(CUSTOMDBCSR_FOUND ${CustomDbcsr_FOUND})
  set(DBCSR_FOUND ${CustomDbcsr_FOUND})
  set(Dbcsr_FOUND ${CustomDbcsr_FOUND})

  if( DBCSR_FOUND )
    message(CHECK_PASS "found")
    message(STATUS "DBCSR library: ${DBCSR_LIBRARY}")
    message(STATUS "DBCSR include directory: ${DBCSR_INCLUDE_DIR}")
  else()
    message(CHECK_FAIL "not found")
  endif()

  if (DBCSR_WITH_CUDA)
    find_package(CUDAToolkit REQUIRED)
    set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -lstdc++")
  else()
    set(DBCSR_WITH_CUDA_PROFILING OFF)
  endif()

  if(NOT DEFINED CMAKE_CUDA_ARCHITECTURES)
    set(CMAKE_CUDA_ARCHITECTURES 80)
  endif()

  if(DBCSR_FOUND AND NOT TARGET DBCSR::DBCSR)
    add_library(DBCSR::DBCSR INTERFACE IMPORTED)
    target_link_libraries(DBCSR::DBCSR INTERFACE "${DBCSR_LIBRARY}")
    target_include_directories(DBCSR::DBCSR BEFORE INTERFACE "${DBCSR_INCLUDE_DIR}")
    target_link_libraries(DBCSR::DBCSR INTERFACE MPI::MPI_Fortran)
    if(TARGET BLAS::BLAS)
      target_link_libraries(DBCSR::DBCSR INTERFACE BLAS::BLAS)
    elseif(TARGET LAPACK::LAPACK)
      target_link_libraries(DBCSR::DBCSR INTERFACE LAPACK::LAPACK)
    endif()

    target_compile_definitions(
      DBCSR::DBCSR
      INTERFACE $<$<BOOL:${DBCSR_WITH_CUDA}>:__DBCSR_ACC>
            $<$<BOOL:${DBCSR_WITH_CUDA}>:__CUDA>
            $<$<BOOL:${DBCSR_WITH_CUDA}>:ARCH_NUMBER=${CMAKE_CUDA_ARCHITECTURES}>
            $<$<BOOL:${DBCSR_WITH_CUDA_PROFILING}>:__CUDA_PROFILING>)
    target_link_libraries(
      DBCSR::DBCSR
      INTERFACE $<$<BOOL:${DBCSR_WITH_CUDA}>:CUDA::cudart>
            $<$<BOOL:${DBCSR_WITH_CUDA}>:CUDA::cuda_driver>
            $<$<BOOL:${DBCSR_WITH_CUDA}>:CUDA::cublas>
            $<$<BOOL:${DBCSR_WITH_CUDA}>:CUDA::nvrtc>
            $<$<BOOL:${DBCSR_WITH_CUDA_PROFILING}>:CUDA::nvToolsExt>)
  endif()

  mark_as_advanced(DBCSR_DETECTION DBCSR_LIBRARY DBCSR_LIBRARY_DIR)

endif()

if(CustomDbscr_FIND_REQUIRED AND NOT TARGET DBCSR::DBCSR)
  message(FATAL_ERROR "Required package DBCSR cannot be found")
endif()

list(POP_BACK CMAKE_MESSAGE_INDENT)

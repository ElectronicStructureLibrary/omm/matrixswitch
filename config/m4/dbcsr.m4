# -*- Autoconf -*-
#
# M4 macros for MatrixSwitch
#
# Copyright (C) 2016 Yann Pouillon
#
# This file is part of the MatrixSwitch software package. For license information,
# please see the COPYING file in the top-level directory of the MatrixSwitch source
# distribution.
#

#
# DBCSR support
#



# MSW_DBCSR_DETECT()
# ------------------
#
# Checks that DBCSR properly works.
#
AC_DEFUN([MSW_DBCSR_DETECT],[
  dnl Init
  msw_dbcsr_ok="unknown"

  dnl Prepare environment
  saved_CPPFLAGS="${CPPFLAGS}"
  saved_FCFLAGS="${FCFLAGS}"
  saved_LIBS="${LIBS}"
  CPPFLAGS="${CPPFLAGS} ${msw_dbcsr_incs}"
  FCFLAGS="${FCFLAGS} ${msw_dbcsr_incs}"
  LIBS="${msw_dbcsr_libs} ${LINALG_LIBS} ${LIBS}"

  dnl Check DBCSR routine
  AC_LANG_PUSH([Fortran])
  AC_MSG_CHECKING([whether the DBCSR library works])
  AC_LINK_IFELSE([AC_LANG_PROGRAM([],
    [[
      use mpi
      use dbcsr_api
      type(dbcsr_type) :: x
      call dbcsr_init_lib(MPI_COMM_WORLD)
      call dbcsr_print(x)
    ]])], [msw_dbcsr_ok="yes"], [msw_dbcsr_ok="no"])
  AC_MSG_RESULT([${msw_dbcsr_ok}])
  AC_LANG_POP([Fortran])

  dnl Restore environment
  CPPFLAGS="${saved_CPPFLAGS}"
  FCFLAGS="${saved_FCFLAGS}"
  LIBS="${saved_LIBS}"
]) # MSW_DBCSR_DETECT

#!/bin/sh

# GNU - Serial - Netlib

../configure \
    --enable-conv \
    --without-mpi \
    CC="gcc" \
    CXX="g++" \
    FC="gfortran" \
    LINALG_LIBS="-llapack -lblas" \
    "$@"

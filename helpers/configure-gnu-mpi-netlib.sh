#!/bin/sh

# GNU - OpenMPI - Netlib

../configure \
    --enable-conv \
    MPICC="mpicc" \
    MPICXX="mpic++" \
    MPIFC="mpifort" \
    LINALG_LIBS="-lscalapack${NETLIB_SUFFIX} -llapack -lblas" \
    "$@"

#!/bin/sh

# GNU - Serial - OpenBLAS

../configure \
    --enable-conv \
    --without-mpi \
    CC="gcc" \
    CXX="g++" \
    FC="gfortran" \
    LINALG_LIBS="-lopenblas" \
    "$@"

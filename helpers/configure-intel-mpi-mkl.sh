#!/bin/sh

# Intel - Intel MPI - MKL

if test "${MKLROOT}" = ""; then
    echo "helper: error: MKLROOT is not defined" >&2
    exit 1
fi

MKL_INCS="-I${MKLROOT}/include"
MKL_LIBS="-Wl,--start-group \
    ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a \
    ${MKLROOT}/lib/intel64/libmkl_sequential.a \
    ${MKLROOT}/lib/intel64/libmkl_core.a \
    ${MKLROOT}/lib/intel64/libmkl_blacs_intelmpi_lp64.a \
    ${MKLROOT}/lib/intel64/libmkl_scalapack_lp64.a \
    -Wl,--end-group -lpthread -lm"

../configure \
    --enable-conv \
    MPICC="mpiicc" \
    MPICXX="mpiicpc" \
    MPIFC="mpiifort" \
    LINALG_INCLUDES="${MKL_INCS}" \
    LINALG_LIBS="${MKL_LIBS}" \
    "$@"

#!/bin/sh

# GNU - OpenMPI - OpenBLAS

../configure \
    --enable-conv \
    MPICC="mpicc" \
    MPICXX="mpic++" \
    MPIFC="mpifort" \
    LINALG_LIBS="-lscalapack${NETLIB_SUFFIX} -lopenblas" \
    "$@"

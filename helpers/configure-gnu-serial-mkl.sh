#!/bin/sh

# GNU - Serial - MKL

if test "${MKLROOT}" = ""; then
    echo "helper: error: MKLROOT is not defined" >&2
    exit 1
fi

MKL_INCS="-m64 -I${MKLROOT}/include"
MKL_LIBS="-Wl,--start-group \
    ${MKLROOT}/lib/intel64/libmkl_gf_lp64.a \
    ${MKLROOT}/lib/intel64/libmkl_sequential.a \
    ${MKLROOT}/lib/intel64/libmkl_core.a \
    -Wl,--end-group -lpthread -lm -ldl"

../configure \
    --enable-conv \
    --without-mpi \
    CC="gcc" \
    CXX="g++" \
    FC="gfortran" \
    LINALG_INCLUDES="${MKL_INCS}" \
    LINALG_LIBS="${MKL_LIBS}" \
    "$@"
